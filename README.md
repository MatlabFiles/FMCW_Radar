# FMCW radar

This repository includes several video files to showcase the performance of our developed portable low-cost FMCW radar. This radar operates using 5V and uses a micro-controller to instantly process the received samples.

The MATLAB folder contains a collection of scripts to plot the stored radar raw data. 
