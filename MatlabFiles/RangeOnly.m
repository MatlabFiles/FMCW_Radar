function [InstantaneousPeakRange]=RangeOnly(filename,numOfSamples,fftBinDist,StartFromFrame)
  global Stop;
  global Pause;
  global MaxNumFrames;
  global NUM_TARGET_DETECT;
  global Frame;

  Stop=false;
  Pause=false;
  Frame=StartFromFrame;

  Data=dlmread(filename,' ', 5, 1);
  [MaxNumFrames,~]=size(Data)

  TimeStep=60/570;
  Time=(0:MaxNumFrames-1)*TimeStep;
  Range=(0:numOfSamples-1)*fftBinDist;

  
  fig=figure;
  set(fig,'KeyPressFcn',@keyDownListener)
  
  Parent = uipanel('Parent',fig,'BorderType','none');
  Parent.TitlePosition = 'centertop';
  
  
  subplot(3,2,[1,2,3,4],'Parent',Parent); grid on; hold on;
  xlabel('Range (m)');
  ylabel('Amplitude (dB)');
  xlim([Range(1) Range(end)]);
  ymax=5+ceil(max(max(Data(:,1:numOfSamples))));
  ylim([0 ymax])
  
  subplot(3,2,5,'Parent',Parent); grid on; hold on;
  xlabel('Time (s)');
  ylabel('Range (m)');
  ylim([0 37]);
  
  subplot(3,2,6,'Parent',Parent); grid on; hold on;
  xlabel('Time (s)');
  ylabel('Range (m)');
  ylim([0 37]);
  
  InstantaneousPeak=ones(MaxNumFrames,3);
%   InstantaneousPeak= Data(:,2*numOfSamples+(1:3))+1;
  
  Color = char({'b','r','k','g','m','c','y','r'});
  while ~Stop
      AmplitudeInformation=Data(Frame,1:numOfSamples);
      PeakInformation=Data(Frame,2*numOfSamples+(1:numOfSamples));
      
      Peak=find(PeakInformation==1);
      if ~isempty(Peak)
        InstantaneousPeak(Frame,1:min(length(Peak),3))=Peak(1:min(length(Peak),3))+1;
      end
      subplot(3,2,[1,2,3,4]), cla;
      plot(Range,AmplitudeInformation,'b')
      plot(Range(Peak),AmplitudeInformation(Peak),'ro')
      
      subplot(3,2,5), cla;
      PeakRange=max(1,Frame-100):Frame;
      xlim(Time([PeakRange(1) PeakRange(1)+100]));
      
      for i=1:3
        plot(Time(PeakRange),Range(InstantaneousPeak(PeakRange,i)),'color',Color(i),'linestyle','none','marker','.')
      end
      
      FirstPeaksIndex(Frame,:)=LogPeakTable(PeakInformation);
      subplot(3,2,6), cla;
      xlim(Time([PeakRange(1) PeakRange(1)+100]));
      
      for i=1:NUM_TARGET_DETECT
        plot(Time(PeakRange),Range(FirstPeaksIndex(PeakRange,i)+1),'color',Color(i),'linestyle','none','marker','.')
      end
       
 

      Parent.Title = strcat('Frame = ',int2str(Frame));
      if Pause
          waitforbuttonpress
      else
          Frame=Frame+1;
          Frame=min(Frame,MaxNumFrames);
          pause(0.08);
      end
  end
  InstantaneousPeakRange=Range(InstantaneousPeak);
end
