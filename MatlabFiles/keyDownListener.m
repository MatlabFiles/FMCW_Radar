function keyDownListener(src,event)
    global Stop;
    global Pause;

    global Frame;
    global MaxNumFrames;

    switch event.Key
        case {'rightarrow' , 'uparrow' }
            Frame=Frame+1;
            Frame=min(Frame,MaxNumFrames);
        case {'leftarrow' , 'downarrow'}
            Frame=Frame-1;
            Frame=max(Frame,1);
        case 'space'
            Pause=~Pause;
        case 'c'
            Stop=true;
        otherwise
            display('undefined key.')
            display('   Press arrows to change Frame.')
            display('   Press Spacebar to pause or c to close.')
    end
end
