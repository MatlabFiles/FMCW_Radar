function RangeDirection(filename,BinOfInterest,fftBinDist,StartFromFrame)

  global Stop;
  global Pause;
  global MaxNumFrames;
  global Frame;
  global NUM_TARGET_DETECT;

  Stop=false;
  Pause=false;
  Frame=StartFromFrame;
  FFTSize= 512;

  Data=dlmread(filename,' ', 5, 1);
  [MaxNumFrames,~]=size(Data)

  TimeStep=60/450;
  Time=(0:MaxNumFrames-1)*TimeStep;
  Range=BinOfInterest*fftBinDist;
  numOfSamples=length(Range);
  AngleRange=-63:63;
  
  fig=figure;
  set(fig,'KeyPressFcn',@keyDownListener)
  
  Parent = uipanel('Parent',fig,'BorderType','none');
  Parent.TitlePosition = 'centertop';
  
  
  subplot(3,4,[5,6,9,10],'Parent',Parent); grid on; hold on;
  colormap(flipud(hot));% colorbar;
  xlim([Range(1) Range(end)]);
  ylim([AngleRange(1) AngleRange(end)]);
  xlabel('Range (m)');
  ylabel('Direction (deg)');
  
  subplot(3,4,[7,8,11,12],'Parent',Parent); grid on; hold on;
  colormap(flipud(hot));% colorbar;
  xlim([Range(1) Range(end)]);
  ylim([AngleRange(1) AngleRange(end)]);
  xlabel('Range (m)');
  ylabel('Direction (deg)');
  
  subplot(3,4,[1,2],'Parent',Parent); grid on; hold on;
  xlabel('Range (m)');
  ylabel('Amplitude (dB)');
  xlim([Range(1) Range(end)]);
  ymax=5+ceil(max(max(Data(:,1:numOfSamples))));
  ylim([0 ymax])
  
  subplot(3,4,3,'Parent',Parent); grid on; hold on;
  xlabel('Time (s)');
  ylabel('Range (m)');
  ylim([0 37]);
  
  subplot(3,4,4,'Parent',Parent); grid on; hold on;
  xlabel('Time (s)');
  ylabel('Direction (deg)');
  ylim([-63 63]);
  
  AngleResolution=3;
  RangeResolution=2;
%   InstantaneousPeak=ones(MaxNumFrames,3);
  InstantaneousPeak= Data(:,2*numOfSamples+FFTSize/2+(1:3))+1;
  InstantaneousDirection=zeros(MaxNumFrames,3);
    
  MaxPlotRange=min(100,MaxNumFrames);
  
  Color = char({'b','r','k','g','m','c','y','r'});
  
  while ~Stop
      AmplitudeInformation=Data(Frame,1:numOfSamples);
      DirectionInformation=Data(Frame,numOfSamples+(1:numOfSamples));
      
      angle_shift=16;
      temp=find(DirectionInformation==180);
      DirectionInformation(temp)=180+angle_shift;
      DirectionInformation=DirectionInformation-angle_shift;
      temp=find(DirectionInformation<0);
      DirectionInformation(temp)=DirectionInformation(temp)+360;
      
      DirectionInformation=round(180/pi*asin(3e8*pi/180*(DirectionInformation-180)./(2*pi*24.125e9*7e-3)));
      
      
      PeakInformation=Data(Frame,2*numOfSamples+(1:FFTSize/2));
      [FirstPeaksIndex(Frame,:),FirstPeaksDirection(Frame,:)]=LogPeakTable(PeakInformation,DirectionInformation);
      Peak=find(PeakInformation==1);

%       if ~isempty(Peak)
%         InstantaneousPeak(Frame,1:min(length(Peak),3))=Peak(1:min(length(Peak),3));
%         InstantaneousDirection(Frame,1:min(length(Peak),3))=DirectionInformation(InstantaneousPeak(Frame,1:min(length(Peak),3)));
%       end
      
      InstantaneousDirection(Frame,1:min(length(Peak),3))=DirectionInformation(InstantaneousPeak(Frame,1:min(length(Peak),3)));
      
      RawMatrix=zeros(length(AngleRange),numOfSamples);
      for i =1:numOfSamples
          RawMatrix(DirectionInformation(i)-AngleRange(1)+1,i,1)=AmplitudeInformation(i);
      end
      
      SmoothedMatrix=SmoothImage(RawMatrix,AngleResolution,RangeResolution);
      
      subplot(3,4,[5,6,9,10]), cla;
      imagesc(Range,AngleRange,RawMatrix)
      plot3(Range(Peak),DirectionInformation(Peak),ones(length(Peak)),'ob')
      
      subplot(3,4,[7,8,11,12]), cla;
      imagesc(Range,AngleRange,SmoothedMatrix)
      plot3(Range(Peak),DirectionInformation(Peak),ones(length(Peak)),'ob')
      
      subplot(3,4,[1,2]), cla;
      plot(Range,AmplitudeInformation,'b')
      plot(Range(Peak),AmplitudeInformation(Peak),'ro')
      
      subplot(3,4,3), cla;
      PeakRange=max(1,Frame-MaxPlotRange):Frame;
      xlim(Time([PeakRange(1) PeakRange(1)+MaxPlotRange-1]));

      for i=1:3
        plot(Time(PeakRange),Range(InstantaneousPeak(PeakRange,i)),'color',Color(i),'linestyle','none','marker','.')
      end
      
      for i=1:NUM_TARGET_DETECT
        plot(Time(PeakRange),Range(FirstPeaksIndex(PeakRange,i)+1),'color',Color(i),'linestyle','none','marker','.')
      end

      subplot(3,4,4), cla;
      PeakRange=max(1,Frame-MaxPlotRange):Frame;
      xlim(Time([PeakRange(1) PeakRange(1)+MaxPlotRange-1]));
      for i=1:3
        plot(Time(PeakRange),InstantaneousDirection(PeakRange,i),'color',Color(i),'linestyle','none','marker','.')
      end
      
      for i=1:NUM_TARGET_DETECT
        plot(Time(PeakRange),FirstPeaksDirection(PeakRange,i),'color',Color(i),'linestyle','none','marker','.')
      end

      Parent.Title = strcat('Frame = ',int2str(Frame));
      if Pause
          waitforbuttonpress
      else
          Frame=Frame+1;
          Frame=min(Frame,MaxNumFrames);
          pause(0.08);
      end
  end
end