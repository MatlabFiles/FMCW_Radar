Data=dlmread('./RadarRawData/RangeDirection_01.txt',' ', 130, 1);
[MaxNumFrames,~]=size(Data)

c=3e8;
f_c=500e3;
tau=250e-6;
beta=250e6;

FFTSize= 512;
fftBinDist= c*f_c*tau/(4*beta)/(FFTSize/2);

Range=(0:255)'*fftBinDist;

numOfSamples=256;
int_range=1:400; Target=2;
Time=(0:length(int_range)-1)*60/250;

DirectionInformation=Data(:,numOfSamples+(1:numOfSamples));
PeakInformation     =Data(:,2*numOfSamples+(1:numOfSamples));

% Shifting the direction information to center them at 0
angle_shift=16;
temp=find(DirectionInformation==180);
DirectionInformation(temp)=180+angle_shift;
DirectionInformation=DirectionInformation-angle_shift;
temp=find(DirectionInformation<0);
DirectionInformation(temp)=DirectionInformation(temp)+360;
DirectionInformation=round(180/pi*asin(3e8*pi/180*(DirectionInformation-180)./(2*pi*24.125e9*7e-3)));


InstantaneousPeak=zeros(MaxNumFrames,3);

for i=1:MaxNumFrames
    Peak=find(PeakInformation(i,:)==1);
    if ~isempty(Peak)
%         keyboard
        InstantaneousPeak(i,1:min(length(Peak),3))=Peak(1:min(length(Peak),3));
        InstantaneousDirection(i,1:min(length(Peak),3))=DirectionInformation(i,InstantaneousPeak(i,1:min(length(Peak),3))+1);
    end
end

figure; clf; 

subplot(2,3,1); hold on;
title('Instan. Measurements')
plot(Time,Range(InstantaneousPeak(int_range,1)+1),'b.')
plot(Time,Range(InstantaneousPeak(int_range,2)+1),'r.')
% plot(Range(InstantaneousPeak(int_range,3)+1),'k.')
ylabel('Range (m)')
xlabel('Time (s)')


subplot(2,3,4); hold on;
plot(Time,InstantaneousDirection(int_range,1),'b.')
plot(Time,InstantaneousDirection(int_range,2),'r.')
% plot(InstantaneousDirection(int_range,3),'k.')
ylabel('Direction (deg)')
xlabel('Time (s)')

subplot(2,3,2); cla; hold on;
title('Associated Measurements')
ylabel('Range (m)')
xlabel('Time (s)')

subplot(2,3,5); cla; hold on;
ylabel('Range (m)')
xlabel('Time (s)')
    
subplot(2,3,3); cla; hold on;
title('Smoothed Measurements')
ylabel('Range (m)')
xlabel('Time (s)')

subplot(2,3,6); cla; hold on;
ylabel('Range (m)')
xlabel('Time (s)')
        
    
for i =int_range
    [FirstPeaksIndex(i,:),FirstPeaksDirection(i,:)]=LogPeakTable(PeakInformation(i,:),DirectionInformation(i,:));
    
    subplot(2,3,2); cla; hold on;
    plot(Time(1:i),Range(FirstPeaksIndex(1:i,1)+1),'b.')
    plot(Time(1:i),Range(FirstPeaksIndex(1:i,2)+1),'r.')
    
    subplot(2,3,5); cla; hold on;
    plot(Time(1:i),FirstPeaksDirection(1:i,1),'b.')
    plot(Time(1:i),FirstPeaksDirection(1:i,2),'r.')
    
    StepKalman()
    
    S=[KalmanFilterData(:).X];
    Kalman_RangeBin(i,:)=S(1,:);
    Kalman_Direction(i,:)=S(3,:)*180/pi;
    
    subplot(2,3,3); cla; hold on;
    plot(Time(1:i),Kalman_RangeBin(1:i,1),'b.')
    plot(Time(1:i),Kalman_RangeBin(1:i,2),'r.')
    
    subplot(2,3,6); cla; hold on;
    plot(Time(1:i),Kalman_Direction(1:i,1),'b-','linewidth',2)
    plot(Time(1:i),Kalman_Direction(1:i,2),'r-','linewidth',2)
    
    pause(0.01)
end

    
    
    
    