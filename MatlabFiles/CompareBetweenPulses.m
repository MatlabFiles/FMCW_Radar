function CompareBetweenPulses(filename,fftBinDist,NumberOfPulsesPerSweep,StartFromFrame)

  global Stop;
  global Pause;
  global MaxNumFrames;
  global Frame;

  Stop=false;
  Pause=false;
  Frame=StartFromFrame;
  FFTSize= 512;

  Data=dlmread(filename,' ', 5, 1);
  [MaxNumFrames,~]=size(Data)
  
  Range=(0:FFTSize/2-1)*fftBinDist;
  numOfSamples=length(Range);
  AngleRange=-63:63;
  
  fig=figure;
  set(fig,'KeyPressFcn',@keyDownListener)
  
  Parent = uipanel('Parent',fig,'BorderType','none');
  Parent.TitlePosition = 'centertop';
  
  subplot(2,1,1,'Parent',Parent); grid on; hold on;
  xlabel('Range (m)');
  ylabel('Direction (deg)');
  xlim([Range(1) Range(end)]);
  ylim([AngleRange(1) AngleRange(end)]);
  
  
  subplot(2,1,2,'Parent',Parent); grid on; hold on;
  xlabel('Range (m)');
  ylabel('Amplitude (dB)');
  xlim([Range(1) Range(end)]);
  ymax=5+ceil(max(max(Data(:,1:numOfSamples))));
  ylim([0 ymax])
  
  while ~Stop
      for i=0:NumberOfPulsesPerSweep-1
          AmplitudeInformation(:,i+1)=Data(Frame,i*numOfSamples+(1:numOfSamples));
          DirectionInformation(:,i+1)=Data(Frame,(NumberOfPulsesPerSweep+i)*numOfSamples+(1:numOfSamples));
      end
      
      subplot(2,1,1), cla;
      for i=1:NumberOfPulsesPerSweep
          plot(Range,DirectionInformation(:,i))
      end
      
      subplot(2,1,2), cla;
      for i=1:NumberOfPulsesPerSweep
          plot(Range,AmplitudeInformation(:,i))
      end
      
      Parent.Title = strcat('Frame = ',int2str(Frame));
      if Pause
          waitforbuttonpress
      else
          Frame=Frame+1;
          Frame=min(Frame,MaxNumFrames);
          pause(0.08);
      end
  end
end
      
      
  
  
  
  