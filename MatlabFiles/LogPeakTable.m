function [FirstPeaksIndex,FirstPeaksDirection]=LogPeakTable(PeakInformation,DirectionInformation)

global TARGET_MEMORY;
global NUM_TARGET_DETECT;
global KalmanFilterData;
global LogTable;

FirstPeaksIndex=zeros(1,NUM_TARGET_DETECT);
FirstPeaksDirection= zeros(1,NUM_TARGET_DETECT);
PeakStart=5;
PeakStop=512/2;
TotalNumDetectedTargets=0;
maxGuardband=15;

Temp_Target_BinIndex=0;
binId=PeakStart;

while (binId < PeakStop)
    if (PeakInformation(binId+1)==1)
      TotalNumDetectedTargets=TotalNumDetectedTargets+1;
      Temp_Target_BinIndex(TotalNumDetectedTargets)=binId;
    end
    binId=binId+1;
end
% Temp_Target_BinIndex
Temp_Target_Assigned=zeros(TotalNumDetectedTargets,1);

if (LogTable(1).VectHits==0)
    for TargetId=1:TotalNumDetectedTargets;
      LogTable(TargetId).BinIndex=Temp_Target_BinIndex(TargetId);
      LogTable(TargetId).VectHits = 1;
    end
else
    InsPos_1 = 1;
    while (LogTable(InsPos_1).BinIndex<512/2)
      LogTable(InsPos_1).VectHits  = bitshift(LogTable(InsPos_1).VectHits,1,'uint32');
      InsPos_1 = InsPos_1 + 1;
    end
    InsPos_1 = 1;
    TargetId = 1;
    while((InsPos_1<=TARGET_MEMORY) && (LogTable(InsPos_1).BinIndex<512/2))
        while (TargetId <= TotalNumDetectedTargets)
            if (InsPos_1==1)
                GuardBandLeft=min(maxGuardband,LogTable(InsPos_1).BinIndex/2);
            else
                GuardBandLeft=min(maxGuardband,(LogTable(InsPos_1).BinIndex-LogTable(InsPos_1-1).BinIndex)/2);
            end
            if (LogTable(InsPos_1).BinIndex<512/2)
                if (InsPos_1<TARGET_MEMORY)
                    GuardBandRight=min(maxGuardband,(LogTable(InsPos_1+1).BinIndex-LogTable(InsPos_1).BinIndex)/2);
                else
                    GuardBandRight=min(maxGuardband,(256-LogTable(InsPos_1).BinIndex)/2);
                end
            else
                GuardBandRight=maxGuardband;
            end

            if (Temp_Target_BinIndex(TargetId)>LogTable(InsPos_1).BinIndex-GuardBandLeft)
                if (Temp_Target_BinIndex(TargetId)<LogTable(InsPos_1).BinIndex+GuardBandRight)
                    %%Target within gate
                    LogTable(InsPos_1).BinIndex=Temp_Target_BinIndex(TargetId);
                    Temp_Target_Assigned(TargetId)=Temp_Target_Assigned(TargetId)+1;
                    if (mod(LogTable(InsPos_1).VectHits,2)==0)
                        LogTable(InsPos_1).VectHits = LogTable(InsPos_1).VectHits+1;
                    end
                    TargetId=TargetId+1;
                else
                    InsPos_1=InsPos_1+1;
                end
            else
                LogTable(InsPos_1+1:end)=LogTable(InsPos_1:end-1);

                LogTable(InsPos_1).BinIndex=Temp_Target_BinIndex(TargetId);
                Temp_Target_Assigned(TargetId)=Temp_Target_Assigned(TargetId)+1;
                LogTable(InsPos_1).VectHits=1;
                LogTable(InsPos_1).KalmanIndex=0;
                InsPos_1=InsPos_1+1;
                TargetId=TargetId+1;
            end

        end
        break;
    end

    for TargetId = 1:TotalNumDetectedTargets
        if (Temp_Target_Assigned(TargetId) == 0)
            error('A detected target is still not assigned')
        end
    end


    InsPos_1=2;
    while((InsPos_1<=TARGET_MEMORY) && (LogTable(InsPos_1).BinIndex<512/2))
        if (LogTable(InsPos_1).BinIndex-LogTable(InsPos_1-1).BinIndex<5)
            if (bitand(LogTable(InsPos_1).VectHits, 1)==1) || (bitand(LogTable(InsPos_1-1).VectHits, 1)==1)

                SumHits_1=0;
                for j=0:9
                    if (bitand(bitshift(LogTable(InsPos_1-1).VectHits,-j), 1)==1)
                        SumHits_1=SumHits_1+1;
                    end
                end
                SumHits_2=0;
                for j=0:9
                    if (bitand(bitshift(LogTable(InsPos_1).VectHits,-j), 1)==1)
                        SumHits_2=SumHits_2+1;
                    end
                end

                if ( (bitand(LogTable(InsPos_1).VectHits, 1)==1) && (bitand(LogTable(InsPos_1-1).VectHits, 1)==1) )
                    error('By construction, if two hits are detected in both logged peaks, the targets are already merged in the previous step.')

                elseif ( (bitand(LogTable(InsPos_1).VectHits, 1)==1) && (bitand(LogTable(InsPos_1-1).VectHits, 1)==0) )
                    LogTable(InsPos_1-1).BinIndex=LogTable(InsPos_1).BinIndex;
                    if (SumHits_2>SumHits_1)
                        LogTable(InsPos_1-1).VectHits=LogTable(InsPos_1).VectHits;
                    else
                        LogTable(InsPos_1-1).VectHits=LogTable(InsPos_1-1).VectHits+1;
                    end

                    if (LogTable(InsPos_1-1).KalmanIndex==0)
                        LogTable(InsPos_1-1).KalmanIndex=LogTable(InsPos_1).KalmanIndex;
                    elseif (LogTable(InsPos_1).KalmanIndex~=0)
                        DeleteKalmanIndex(LogTable(InsPos_1).KalmanIndex)
                    end

                    LogTable(InsPos_1:end-1)=LogTable(InsPos_1+1:end);
                    LogTable(end).BinIndex=512/2;
                    LogTable(end).VectHits=0;
                    LogTable(end).KalmanIndex=0;

                elseif ( (bitand(LogTable(InsPos_1).VectHits, 1)==0) && (bitand(LogTable(InsPos_1-1).VectHits, 1)==1) )
                    if (SumHits_2>SumHits_1)
                        LogTable(InsPos_1-1).VectHits=LogTable(InsPos_1).VectHits+1;
                    else
                        LogTable(InsPos_1-1).VectHits=LogTable(InsPos_1-1).VectHits;
                    end

                    if (LogTable(InsPos_1-1).KalmanIndex==0)
                        LogTable(InsPos_1-1).KalmanIndex=LogTable(InsPos_1).KalmanIndex;
                    elseif  (LogTable(InsPos_1).KalmanIndex~=0)
                        DeleteKalmanIndex(LogTable(InsPos_1).KalmanIndex)
                    end

                    LogTable(InsPos_1:end-1)=LogTable(InsPos_1+1:end);
                    LogTable(end).BinIndex=512/2;
                    LogTable(end).VectHits=0;
                    LogTable(end).KalmanIndex=0;

                end
            else
                error('By construction, if no hit is detected in both logged peaks, the targets should not be less than 5 bins away.')
            end
        else
            InsPos_1=InsPos_1+1;
        end
    end


    MemId = 1;
    while ((MemId <= TARGET_MEMORY) && (LogTable(MemId).BinIndex<512/2))
      SumHits_1=0;
      SumDel=0;
      for j=0:4
          if (bitand(bitshift(LogTable(MemId).VectHits,-j), 1)==1)
              SumHits_1=SumHits_1+1;
          end
      end
      SumHits_2=SumHits_1;
      for j=5:9
          if (bitand(bitshift(LogTable(MemId).VectHits,-j), 1)==1)
              SumHits_2=SumHits_2+1;
          end
      end
      SumHits_3=SumHits_2;
      for j=10:31
          if (bitand(bitshift(LogTable(MemId).VectHits,-j), 1)==1)
                  SumHits_3=SumHits_3+1;
          end
      end

      for j=0:11
          if (bitand(bitshift(LogTable(MemId).VectHits,-j), 1)==0)
              SumDel=SumDel+1;
          end
      end

      if ((SumHits_1>2) || (SumHits_2 > 4) || (SumHits_3 > 15))
          if (LogTable(MemId).KalmanIndex==0)
              LogTable(MemId).KalmanIndex=AssignKalmanIndex();
          end

          if (bitand(LogTable(MemId).VectHits, 1)==1)
              KalmanFilterData(LogTable(MemId).KalmanIndex).M_BinIndex=LogTable(MemId).BinIndex;
              FirstPeaksIndex(LogTable(MemId).KalmanIndex)=LogTable(MemId).BinIndex;
              if exist('DirectionInformation','var') 
                KalmanFilterData(LogTable(MemId).KalmanIndex).M_Direction=DirectionInformation(LogTable(MemId).BinIndex+1);
                FirstPeaksDirection(LogTable(MemId).KalmanIndex)=DirectionInformation(LogTable(MemId).BinIndex+1);
              else
                  KalmanFilterData(LogTable(MemId).KalmanIndex).M_Direction=0;
                  FirstPeaksDirection(LogTable(MemId).KalmanIndex)=0;
              end
          else
              KalmanFilterData(LogTable(MemId).KalmanIndex).M_BinIndex=0;
              KalmanFilterData(LogTable(MemId).KalmanIndex).M_Direction=0;
              FirstPeaksIndex(LogTable(MemId).KalmanIndex)=0;
              FirstPeaksDirection(LogTable(MemId).KalmanIndex)=0;

          end

        MemId=MemId+1;
      elseif (SumDel==12)

        if (LogTable(MemId).KalmanIndex~=0)
            DeleteKalmanIndex(LogTable(MemId).KalmanIndex)
        end
        LogTable(MemId:end-1)=LogTable(MemId+1:end);


        LogTable(end).BinIndex=512/2;
        LogTable(end).VectHits=0;
        LogTable(end).KalmanIndex=0;
      else
        MemId=MemId+1;
      end
    end
end

% [num2str([LogTable(1:10).BinIndex]') , num2str(dec2bin([LogTable(1:10).VectHits]'))]
% FirstPeaksIndex(1:4)
% [LogTable(1:4).KalmanIndex]
