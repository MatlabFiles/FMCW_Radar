function cMatrix=SmoothImage(Matrix,AngleResolution,RangeResolution)
cMatrix=Matrix;
for j=1:AngleResolution
%     keyboard
    cMatrix=cMatrix+[cMatrix(j+1:end,:);cMatrix(1:j,:)]+[cMatrix(end-j+1:end,:);cMatrix(1:end-j,:)];
end


switch (AngleResolution)
    case 2,
        cMatrix=cMatrix/2;
    case 3,
        cMatrix=cMatrix/3;
    case 4,
        cMatrix=cMatrix/7;
    case 5,
        cMatrix=cMatrix/15;
    case 6,
        cMatrix=cMatrix/35;
    case 7,
        cMatrix=cMatrix/87;
end


for j=1:RangeResolution
    cMatrix=cMatrix+[cMatrix(:,j+1:end),cMatrix(:,1:j)]+[cMatrix(:,end-j+1:end),cMatrix(:,1:end-j)];
end

cMatrix=cMatrix/max(max(cMatrix))*10;
