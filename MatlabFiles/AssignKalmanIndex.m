function AssignedNumber=AssignKalmanIndex()
global NUM_TARGET_DETECT;
global KalmanFilterData;
% keyboard
for k=1:NUM_TARGET_DETECT
    if (KalmanFilterData(k).Activated==0)
        break;
    end
end

if ((k<NUM_TARGET_DETECT) || (KalmanFilterData(NUM_TARGET_DETECT).Activated==0))
    KalmanFilterData(k).Activated=1;
    AssignedNumber=k;
else
    display('All kalmans activated')
end