clc; clear all; close all;
c=3e8;
f_c=500e3;
tau=250e-6;
beta=250e6;

FFTSize= 512;
FFTSize_SlowTime= 16;
fftBinDist= c*f_c*tau/(4*beta)/(FFTSize/2);

global TARGET_MEMORY;
global NUM_TARGET_DETECT;
global KalmanFilterData;
global LogTable;

LogTable_structinit = struct( ...
    'BinIndex'    , 256, ...
    'VectHits'    , 0, ...
    'KalmanIndex' , 0 ...
    );
KF_structinit = struct( ...
        'Activated'  , 0, ...
        'X'          , [0;0;0;0], ...
        'P'          , [0.5 0 0 0; 0 0.5 0 0; 0 0 1 0; 0 0 0 1], ...
        'M_BinIndex' , 0, ...ccccc
        'M_Direction', 180, ...
        'Offcounter' , 100  ...
    );

TARGET_MEMORY     = 60 ;
NUM_TARGET_DETECT = 8 ;

LogTable = LogTable_structinit;
LogTable (2:NUM_TARGET_DETECT)=LogTable_structinit;
KalmanFilterData = KF_structinit;
KalmanFilterData (2:NUM_TARGET_DETECT)=KF_structinit;


% StartFromFrame is a parameter to start from a specific frame
% The function parameters are specified in the loaded file

% RawData(filename,NumberOfPulsesPerSweep,NumberOfSamplesPerPulse,StartFromFrame)
RawData('./RadarRawData/RawData_TimeSamples_00.txt',1,185,1); close all;
RawData('./RadarRawData/RawData_TimeSamples_01.txt',1,125,1); close all;
RawData('./RadarRawData/RawData_TimeSamples_02.txt',1,125,1); close all;
RawData('./RadarRawData/RawData_TimeSamples_03.txt',8,125,1); close all;
RawData('./RadarRawData/RawData_TimeSamples_04.txt',8,125,1); close all;
RawData('./RadarRawData/RawData_TimeSamples_05.txt',8,125,1); close all;
RawData('./RadarRawData/RawData_TimeSamples_06.txt',16,125,1);close all;

% RangeOnly(filename,numOfSamples,fftBinDist,StartFromFrame)
RangeOnly('./RadarRawData/RangeOnly_01.txt',256,fftBinDist,1);close all;

% RangeDirection(filename,BinOfInterest,fftBinDist,StartFromFrame)
RangeDirection('./RadarRawData/RangeDirection_01.txt',0:255,fftBinDist,130);close all;

% CompareBetweenPulses(filename,fftBinDist,NumberOfPulsesPerSweep,StartFromFrame)
CompareBetweenPulses('./RadarRawData/FFT_Direction_Pulses_SameSweep.txt',fftBinDist,8,1);close all;

% SlowTimeProcessing(filename,BinOfInterest,fftBinDist,SlowTimeSize,T_PRI,StartFromFrame)
SlowTimeProcessing('./RadarRawData/SlowtimeProcessing_01.txt',0:255,fftBinDist,16,1.5e-3,1);close all;

% MovementDetection(filename,TargetRange,fftBinDist,numOfSamples,DeltaThreshold,StartFromFrame)
MovementDetection('./RadarRawData/MovementDetection.txt',1:256,fftBinDist,256,10,2);close all;

% still in draft state
KalmanFilter_Performance;close all;
