function StepKalman()
global KalmanFilterData;
global NUM_TARGET_DETECT;

% R=[(2)^2 0; 0 (20*pi/180)^2];
% Q=[1 0 0 0; 0 (1.2)^2 0 0; 0 0 30*pi/180 0; 0 0 0 20*pi/180];


% R=[(2)^2 0; 0 (20*pi/180)^2];
% Q=[0.5 0 0 0; 0 (0.2)^2 0 0; 0 0 30*pi/180 0; 0 0 0 20*pi/180];

% R=[(2)^2 0; 0 (20*pi/180)^2];
% Q=[0.5 0 0 0; 0 (0.2)^2 0 0; 0 0 2*pi/180 0; 0 0 0 2*pi/180];

% R=[1 0; 0 (20*pi/180)^2];
% Q=[0.05 0 0 0; 0 0.05 0 0; 0 0 1*pi/180 0; 0 0 0 1*pi/180];




% R=[0.3 0; 0 0.6];
% Q=[7 0 0 0; 0 7 0 0; 0 0 0.1 0; 0 0 0 0.1];
t=60/250;
H= [1 0 0 0; 0 0 1 0];
amax=asin(3e8/(2*24.125e9*7e-3));
for k=1:NUM_TARGET_DETECT
    if (KalmanFilterData(k).Activated == 1)
        R=[1 0; 0 10*pi/180];
        Q=[0.1 0 0 0; 0 0.1 0 0; 0 0 2*pi/180 0; 0 0 0 2*pi/180];
        Range=KalmanFilterData(k).M_BinIndex*0.1465;
        Direction=KalmanFilterData(k).M_Direction;
        Direction=Direction*pi/180;
        
        if (abs(Direction- KalmanFilterData(k).X(3))>pi/2) && (abs(KalmanFilterData(k).X(3))>pi/6) && (abs(Direction) > pi/6)
            if (Direction<0)
                Direction= Direction - (-amax) + amax;
            else
                Direction= amax - Direction + (-amax);
            end
        end
        
        if (KalmanFilterData(k).Offcounter>15)
            KalmanFilterData(k).X=[Range;0;Direction;0]; % Vrange ; VAngle
            KalmanFilterData(k).P=[0.5 0 0 0; 0 0.5 0 0; 0 0 1 0; 0 0 0 1];
            KalmanFilterData(k).Offcounter=0;
        end

        A = [1 t 0 0; 0 1 0 0; 0 0 1 t; 0 0 0 1];
        Z(1,1)=Range;
        Z(2,1)=Direction;

        E = Z - H * A * KalmanFilterData(k).X;

%         if (E(1)^2 > 10*KalmanFilterData(k).P(1,1)) || (E(2)^2 > 10*KalmanFilterData(k).P(2,2))
        if ((Direction == 0) && (Range == 0)) ...
            || (abs(E(1)) > 1*KalmanFilterData(k).P(1,1)) ...
            || (abs(E(2)) > 1*KalmanFilterData(k).P(3,3))
            
            KalmanFilterData(k).Offcounter=KalmanFilterData(k).Offcounter +1;
            A = [1 t/10 0 0; 0 1 0 0; 0 0 1 t/5; 0 0 0 1];
            KalmanFilterData(k).P=A*KalmanFilterData(k).P*A'+Q;
            KalmanFilterData(k).X=A*KalmanFilterData(k).X;
        else
            KalmanFilterData(k).Offcounter=0;
            KalmanFilterData(k).P=A*KalmanFilterData(k).P*A'+Q;
            K=KalmanFilterData(k).P*H'*(H*KalmanFilterData(k).P*H'+R)^(-1);
            KalmanFilterData(k).X=A*KalmanFilterData(k).X+K*E;
            KalmanFilterData(k).P=KalmanFilterData(k).P-K*H*KalmanFilterData(k).P;
        end
    end
end