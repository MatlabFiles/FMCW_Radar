function MovementDetection(filename,TargetRange,fftBinDist,numOfSamples,DeltaThreshold,StartFromFrame)

  global Stop;
  global Pause;
  global MaxNumFrames;
  global Frame;

  Stop=false;
  Pause=false;
  Frame=StartFromFrame;

  Data=dlmread(filename,' ', 5, 1);
  [MaxNumFrames,~]=size(Data)
  
  Amplitude=Data(:,1:numOfSamples);
  for c=1:4
    Phase(:,:,c)  =Data(:,(1:numOfSamples)+c*numOfSamples);
  end
  
  TimeStep=60/250;
  Time=(0:MaxNumFrames-1)*TimeStep;
  Range=(0:numOfSamples-1)*fftBinDist;
  
  
  fig=figure;
  set(fig,'KeyPressFcn',@keyDownListener)
  
  Parent = uipanel('Parent',fig,'BorderType','none');
  Parent.TitlePosition = 'centertop';
  
  subplot(2,2,1,'Parent',Parent); grid on; hold on;
  xlabel('Range (m)');
  ylabel('Amplitude (dB)');
  xlim([Range(1) Range(end)]);
  ymax=5+ceil(max(max(Amplitude)));
  ylim([0 ymax])
  
  subplot(2,2,2,'Parent',Parent); grid on; hold on;
  xlabel('Range (m)');
  ylabel('Phase (deg)');
  xlim([Range(1) Range(end)]);
  ylim([0 180])
  
  
  subplot(2,2,3,'Parent',Parent); grid on; hold on;
  xlabel('Time (s)');
  ylabel('A_{MS}  (dB^2)');
  
  
  subplot(2,2,4,'Parent',Parent); grid on; hold on;
  xlabel('Time (s)');
  ylabel('\Phi_{MS} (deg^2)');
  
  while ~Stop
    
    Amp_ref=Amplitude(Frame-1,TargetRange);
    Amp_current=Amplitude(Frame,TargetRange);
    Amp_num =abs(Amp_current-Amp_ref);
    
    subplot(2,2,1);cla;
    plot(Range(TargetRange),Amp_ref,'b')
    plot(Range(TargetRange),Amp_current,'r')
    plot(Range(TargetRange),Amp_num,'k')

    subplot(2,2,2); cla;
    for c=1:4
        Phase_ref=Phase(Frame-1,TargetRange,c);
        Phase_current=Phase(Frame,TargetRange,c);
        Delta_Phase(c,:)=min(abs(Phase_current-Phase_ref),360-abs(Phase_current-Phase_ref)).*(Amp_current>DeltaThreshold);
        plot(Range(TargetRange),Delta_Phase(c,:));
    end

    PhiMs(Frame,1)=sum(min(Delta_Phase.^2))/length(find(Amp_current>DeltaThreshold));
    PhiMs(Frame,2)=sum(mean(Delta_Phase.^2))/length(find(Amp_current>DeltaThreshold));
    AmpMS(Frame,1)=mean(Amp_num.^2);

    subplot(2,2,3);cla; 
    plot(Time(1:Frame),AmpMS(1:Frame,1)); 
    
    subplot(2,2,4);cla; 
    plot(Time(1:Frame),PhiMs(1:Frame,1));
    plot(Time(1:Frame),PhiMs(1:Frame,2));
    
    

    Parent.Title = strcat('Frame = ',int2str(Frame));
    if Pause
        waitforbuttonpress
        Frame=max(Frame,2);
    else
        Frame=Frame+1;
        Frame=min(Frame,MaxNumFrames);
        pause(0.08);
    end
  end
  