function RawData=RawData(filename,NumberOfPulsesPerSweep,NumberOfSamplesPerPulse,StartFromFrame)
  global Stop;
  global Pause;
  global MaxNumFrames;
  global Frame;

  Stop=false;
  Pause=false;
  Frame=StartFromFrame;

  Data=dlmread(filename,' ', 12, 0);

  [numRows,~]=size(Data);
  
  MaxNumFrames=numRows/(NumberOfPulsesPerSweep*NumberOfSamplesPerPulse);
  MaxNumFrames

  Time=2*(0:NumberOfPulsesPerSweep*NumberOfSamplesPerPulse-1);
  
  fig=figure;
  set(fig,'KeyPressFcn',@keyDownListener)

  Parent = uipanel('Parent',fig,'BorderType','none');
  Parent.TitlePosition = 'centertop';

  subplot(2,2,1,'Parent',Parent); grid on; hold on;
  ylim([-2.5 2.5]);
  xlabel('Time (us)');
  ylabel('Voltage (normlized)');
  title('Receiver 1 I')

  subplot(2,2,2,'Parent',Parent); grid on; hold on;
  ylim([-2.5 2.5]);
  xlabel('Time (us)');
  ylabel('Voltage (normlized)');
  title('Receiver 1 Q')

  subplot(2,2,3,'Parent',Parent); grid on; hold on;
  ylim([-2.5 2.5]);
  xlabel('Time (us)');
  ylabel('Voltage (normlized)');
  title('Receiver 2 I')

  subplot(2,2,4,'Parent',Parent); grid on; hold on;
  ylim([-2.5 2.5]);
  xlabel('Time (us)');
  ylabel('Voltage (normlized)');
  title('Receiver 2 Q')




  while ~Stop
      shift=(Frame-1)*NumberOfPulsesPerSweep*NumberOfSamplesPerPulse;
      
      RawData=zeros(NumberOfPulsesPerSweep*NumberOfSamplesPerPulse,4);
      for i=1:4
        RawData(:,i)=Data(shift+(1:NumberOfPulsesPerSweep*NumberOfSamplesPerPulse),i);
        RawData(:,i)=RawData(:,i)/max(max(abs(RawData(:,i))))*2.5;
        subplot(2,2,i); cla; plot(Time,RawData(:,i))
      end
      
      Parent.Title = strcat('Frame = ',int2str(Frame));

      if Pause
          waitforbuttonpress
      else
          Frame=Frame+1;
          Frame=min(Frame,MaxNumFrames);
          pause(0.1);
      end
  end
end
