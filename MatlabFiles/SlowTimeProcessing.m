function SlowTimeProcessing(filename,BinOfInterest,fftBinDist,SlowTimeSize,T_PRI,StartFromFrame)
    
  global Stop;
  global Pause;
  global MaxNumFrames;
  global Frame;

  Stop=false;
  Pause=false;
  Frame=StartFromFrame;
  FFTSize= 512;
  
  Data=dlmread(filename,' ', 5, 1);
  [MaxNumFrames,~]=size(Data)
  
  TimeStep=60/570;
  Time=(0:MaxNumFrames-1)*TimeStep;
  Range=BinOfInterest*fftBinDist;
  numOfSamples=length(Range);
  VelocityMax=3e8/(4*24.125e9*T_PRI);
  VelocityRange= (-0.5+1/SlowTimeSize:1/SlowTimeSize:0.5)*2*VelocityMax;
  AngleRange=-63:63;
  
  
  fig=figure;
  set(fig,'KeyPressFcn',@keyDownListener)
  
  Parent = uipanel('Parent',fig,'BorderType','none');
  Parent.TitlePosition = 'centertop';
  
  
  
  subplot(4,4,[3,4,7,8],'Parent',Parent); grid on; hold on;
  colormap(flipud(hot));% colorbar;
  xlim([Range(1) Range(end)]);
  ylim([AngleRange(1) AngleRange(end)]);
  xlabel('Range (m)');
  ylabel('Direction (deg)');
  
  subplot(4,4,[11,12,15,16],'Parent',Parent); grid on; hold on;
  colormap(flipud(hot));% colorbar;
  xlim([Range(1) Range(end)]);
  ylim([VelocityRange(1) VelocityRange(end)]);
  xlabel('Range (m)');
  ylabel('Velocity (m/s)');
  
  subplot(4,4,[1,2,5,6],'Parent',Parent); grid on; hold on;
  xlabel('Range (m)');
  ylabel('Amplitude (dB)');
  xlim([Range(1) Range(end)]);
  ymax=5+ceil(max(max(Data(:,SlowTimeSize*numOfSamples+(1:numOfSamples)))));
  ylim([0 ymax])
  
  subplot(4,4,[9,10],'Parent',Parent); grid on; hold on;
  xlabel('Time (s)');
  ylabel('Range (m)');
  ylim([0 37]);
  
  subplot(4,4,[13,14],'Parent',Parent); grid on; hold on;
  xlabel('Time (s)');
  ylabel('Direction (deg)');
  ylim([-63 63]);
  
  AngleResolution=3;
  RangeResolution=2;
  InstantaneousPeak=ones(MaxNumFrames,3);
  InstantaneousDirection=zeros(MaxNumFrames,3);
  
  MaxPlotRange=min(100,MaxNumFrames);
  
  Color = char({'b','r','k'});
  
  while ~Stop
      VelocityMatrix=zeros(SlowTimeSize,numOfSamples);
      for j = 1:SlowTimeSize
        for i = 1:numOfSamples
          VelocityMatrix(j,i)=Data(Frame,(j-1)*numOfSamples+i);
        end
      end
      
      AmplitudeInformation=Data(Frame,SlowTimeSize*numOfSamples+(1:numOfSamples));
      DirectionInformation=Data(Frame,(SlowTimeSize+1)*numOfSamples+(1:numOfSamples));
      PeakInformation=Data(Frame,(SlowTimeSize+2)*numOfSamples+(1:FFTSize/2));
      
      Peak=find(PeakInformation==1);

      if ~isempty(Peak)
        InstantaneousPeak(Frame,1:min(length(Peak),3))=Peak(1:min(length(Peak),3))+1;
        InstantaneousDirection(Frame,1:min(length(Peak),3))=DirectionInformation(InstantaneousPeak(Frame,1:min(length(Peak),3)));
      end
      InstantaneousDirection(Frame,1:min(length(Peak),3))=DirectionInformation(InstantaneousPeak(Frame,1:min(length(Peak),3)));
      
      
      RawMatrix=zeros(length(AngleRange),numOfSamples);
      for i =1:numOfSamples
          RawMatrix(DirectionInformation(i)-AngleRange(1)+1,i,1)=AmplitudeInformation(i);
      end
      
      SmoothedMatrix=SmoothImage(RawMatrix,AngleResolution,RangeResolution);
      
      
      subplot(4,4,[11,12,15,16]), cla;
      imagesc(Range,VelocityRange,VelocityMatrix)
      
      
      subplot(4,4,[3,4,7,8]), cla;
      imagesc(Range,AngleRange,SmoothedMatrix)
      plot3(Range(Peak),DirectionInformation(Peak),ones(length(Peak)),'ob')
      
      subplot(4,4,[1,2,5,6]), cla;
      plot(Range,AmplitudeInformation,'b')
      plot(Range(Peak),AmplitudeInformation(Peak),'ro')
      
      subplot(4,4,[9,10]), cla;
      PeakRange=max(1,Frame-MaxPlotRange):Frame;
      xlim(Time([PeakRange(1) PeakRange(1)+MaxPlotRange-1]));

      for i=1:3
        plot(Time(PeakRange),Range(InstantaneousPeak(PeakRange,i)),'color',Color(i),'linestyle','none','marker','.')
      end      
      
      
      subplot(4,4,[13,14]), cla;
      PeakRange=max(1,Frame-MaxPlotRange):Frame;
      xlim(Time([PeakRange(1) PeakRange(1)+MaxPlotRange-1]));
      for i=1:3
        plot(Time(PeakRange),InstantaneousDirection(PeakRange,i),'color',Color(i),'linestyle','none','marker','.')
      end
      

      Parent.Title = strcat('Frame = ',int2str(Frame));
      if Pause
          waitforbuttonpress
      else
          Frame=Frame+1;
          Frame=min(Frame,MaxNumFrames);
          pause(0.08);
      end
  end
  
end
